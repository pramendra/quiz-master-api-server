class CreateV1Assessments < ActiveRecord::Migration[5.0]
  def change
    create_table :v1_assessments do |t|
      t.integer :user_id
      t.integer :question_id
      t.string :reply

      t.timestamps
    end
  end
end
