class CreateV1Answers < ActiveRecord::Migration[5.0]
  def change
    create_table :v1_answers do |t|
      t.integer :question_id
      t.string :answer
      t.integer :answer_type

      t.timestamps
    end
  end
end
