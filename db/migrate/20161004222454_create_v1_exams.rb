class CreateV1Exams < ActiveRecord::Migration[5.0]
  def change
    create_table :v1_exams do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
