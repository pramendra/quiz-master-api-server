class CreateV1Questions < ActiveRecord::Migration[5.0]
  def change
    create_table :v1_questions do |t|
      t.integer :exam_id
      t.string :title
      t.boolean :disabled

      t.timestamps
    end
  end
end
