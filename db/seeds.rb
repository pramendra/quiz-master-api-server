User.destroy_all
V1::Exam.destroy_all
V1::Question.destroy_all
V1::Answer.destroy_all
V1::Assessment.destroy_all

# User.first.exams.first.questions.first
# User.first.exams.first.questions.first.answer

admin = User.create!({ email: 'seeduser@local.com', password: '123456', is_admin: true })
exam = admin.exams.create!(name: "mid term exam")
question = admin.exams.first.questions.create!(title: "is how much")
answer = admin.exams.first.questions.first.create_answer!(answer: "14", answer_type: false)
assessment = admin.exams.first.questions.first.create_assessment(user: admin, reply: "4")
admin.save

# puts exams.id
# exams.map {|exam| put exam.id}


# User.create!([
#   { email: 'user1@local.com', password: '123456', is_admin: true },
#   { email: 'user2@local.com', password: '123456' },
#   { email: 'user3@local.com', password: '123456' },
# ])
#
# V1::Exam.destroy_all
# V1::Exam.create!([
#   { name: 'first class test'},
#   { name: 'second class  test'}
# ])
