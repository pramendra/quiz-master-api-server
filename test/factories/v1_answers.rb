FactoryGirl.define do
  factory :v1_answer, class: 'V1::Answer' do
    question_id 1
    answer "MyString"
    answer_type 1
  end
end
