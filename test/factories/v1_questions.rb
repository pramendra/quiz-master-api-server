FactoryGirl.define do
  factory :v1_question, class: 'V1::Question' do
    exam_id 1
    title "MyString"
    disabled false
  end
end
