FactoryGirl.define do
  factory :v1_assessment, class: 'V1::Assessment' do
    user_id 1
    question_id 1
    reply "MyString"
  end
end
