require 'test_helper'

class V1::ExamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @v1_exam = v1_exams(:one)
  end

  test "should get index" do
    get v1_exams_url, as: :json
    assert_response :success
  end

  test "should create v1_exam" do
    assert_difference('V1::Exam.count') do
      post v1_exams_url, params: { v1_exam: { name: @v1_exam.name, user_id: @v1_exam.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show v1_exam" do
    get v1_exam_url(@v1_exam), as: :json
    assert_response :success
  end

  test "should update v1_exam" do
    patch v1_exam_url(@v1_exam), params: { v1_exam: { name: @v1_exam.name, user_id: @v1_exam.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy v1_exam" do
    assert_difference('V1::Exam.count', -1) do
      delete v1_exam_url(@v1_exam), as: :json
    end

    assert_response 204
  end
end
