require 'test_helper'

class V1::QuestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @v1_question = v1_questions(:one)
  end

  test "should get index" do
    get v1_questions_url, as: :json
    assert_response :success
  end

  test "should create v1_question" do
    assert_difference('V1::Question.count') do
      post v1_questions_url, params: { v1_question: { disabled: @v1_question.disabled, exam_id: @v1_question.exam_id, title: @v1_question.title } }, as: :json
    end

    assert_response 201
  end

  test "should show v1_question" do
    get v1_question_url(@v1_question), as: :json
    assert_response :success
  end

  test "should update v1_question" do
    patch v1_question_url(@v1_question), params: { v1_question: { disabled: @v1_question.disabled, exam_id: @v1_question.exam_id, title: @v1_question.title } }, as: :json
    assert_response 200
  end

  test "should destroy v1_question" do
    assert_difference('V1::Question.count', -1) do
      delete v1_question_url(@v1_question), as: :json
    end

    assert_response 204
  end
end
