require 'test_helper'

class V1::AnswersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @v1_answer = v1_answers(:one)
  end

  test "should get index" do
    get v1_answers_url, as: :json
    assert_response :success
  end

  test "should create v1_answer" do
    assert_difference('V1::Answer.count') do
      post v1_answers_url, params: { v1_answer: { answer: @v1_answer.answer, answer_type: @v1_answer.answer_type, question_id: @v1_answer.question_id } }, as: :json
    end

    assert_response 201
  end

  test "should show v1_answer" do
    get v1_answer_url(@v1_answer), as: :json
    assert_response :success
  end

  test "should update v1_answer" do
    patch v1_answer_url(@v1_answer), params: { v1_answer: { answer: @v1_answer.answer, answer_type: @v1_answer.answer_type, question_id: @v1_answer.question_id } }, as: :json
    assert_response 200
  end

  test "should destroy v1_answer" do
    assert_difference('V1::Answer.count', -1) do
      delete v1_answer_url(@v1_answer), as: :json
    end

    assert_response 204
  end
end
