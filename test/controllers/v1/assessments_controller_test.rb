require 'test_helper'

class V1::AssessmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @v1_assessment = v1_assessments(:one)
  end

  test "should get index" do
    get v1_assessments_url, as: :json
    assert_response :success
  end

  test "should create v1_assessment" do
    assert_difference('V1::Assessment.count') do
      post v1_assessments_url, params: { v1_assessment: { question_id: @v1_assessment.question_id, reply: @v1_assessment.reply, user_id: @v1_assessment.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show v1_assessment" do
    get v1_assessment_url(@v1_assessment), as: :json
    assert_response :success
  end

  test "should update v1_assessment" do
    patch v1_assessment_url(@v1_assessment), params: { v1_assessment: { question_id: @v1_assessment.question_id, reply: @v1_assessment.reply, user_id: @v1_assessment.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy v1_assessment" do
    assert_difference('V1::Assessment.count', -1) do
      delete v1_assessment_url(@v1_assessment), as: :json
    end

    assert_response 204
  end
end
