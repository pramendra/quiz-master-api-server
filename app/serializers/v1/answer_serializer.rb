class V1::AnswerSerializer < ActiveModel::Serializer
  attributes :id, :question_id, :answer, :answer_type
end
