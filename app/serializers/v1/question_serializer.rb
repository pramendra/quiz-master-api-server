class V1::QuestionSerializer < ActiveModel::Serializer
  attributes :id, :exam_id, :title, :disabled
end
