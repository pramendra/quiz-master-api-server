class V1::AssessmentSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :question_id, :reply
end
