class V1::Assessment < ApplicationRecord
  belongs_to :user, class_name: "User"
  belongs_to :question, class_name: "V1::Question"
end
