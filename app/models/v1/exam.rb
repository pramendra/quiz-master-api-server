class V1::Exam < ApplicationRecord
  belongs_to :user, class_name: "User"
  has_many :questions, class_name: "V1::Question"
end
