class V1::Answer < ApplicationRecord
  belongs_to :question, class_name: "V1::Question"
end
