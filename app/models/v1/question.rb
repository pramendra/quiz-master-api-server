class V1::Question < ApplicationRecord
  belongs_to :exam, class_name: "V1::Exam"
  has_one :answer, class_name: "V1::Answer"
  has_one :assessment, class_name: "V1::Assessment"

end
