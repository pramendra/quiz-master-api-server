class V1::AssessmentsController < ApplicationController
  before_action :set_v1_assessment, only: [:show, :update, :destroy]

  # GET /v1/assessments
  def index
    @v1_assessments = V1::Assessment.all

    render json: @v1_assessments
  end

  # GET /v1/assessments/1
  def show
    render json: @v1_assessment
  end

  # POST /v1/assessments
  def create
    @v1_assessment = V1::Assessment.new(v1_assessment_params)

    if @v1_assessment.save
      render json: @v1_assessment, status: :created, location: @v1_assessment
    else
      render json: @v1_assessment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/assessments/1
  def update
    if @v1_assessment.update(v1_assessment_params)
      render json: @v1_assessment
    else
      render json: @v1_assessment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /v1/assessments/1
  def destroy
    @v1_assessment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_v1_assessment
      @v1_assessment = V1::Assessment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def v1_assessment_params
      params.require(:data).permit(:user_id, :question_id, :reply)
    end
end
