class V1::ExamsController < ApplicationController
  before_action :set_v1_exam, only: [:show, :update, :destroy]

  # GET /v1/exams
  def index
    @v1_exams = V1::Exam.all

    render json: @v1_exams
  end

  # GET /v1/exams/1
  def show
    render json: @v1_exam
  end

  # POST /v1/exams
  def create
    @v1_exam = V1::Exam.new(v1_exam_params)

    if @v1_exam.save
      render json: @v1_exam, status: :created, location: @v1_exam
    else
      render json: @v1_exam.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/exams/1
  def update
    if @v1_exam.update(v1_exam_params)
      render json: @v1_exam
    else
      render json: @v1_exam.errors, status: :unprocessable_entity
    end
  end

  # DELETE /v1/exams/1
  def destroy
    @v1_exam.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_v1_exam
      @v1_exam = V1::Exam.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def v1_exam_params
      params.require(:data).permit(:name, :user_id)
    end
end
