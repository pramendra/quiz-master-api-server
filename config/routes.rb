Rails.application.routes.draw do
  namespace :v1 do
    resources :assessments
  end
  namespace :v1 do
    resources :answers
  end
  namespace :v1 do
    resources :questions
  end
  namespace :v1 do
    resources :exams
  end
  namespace :v1 do
    end
  # devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_for :user, only: []
  namespace :v1, defaults: { format: :json } do
    resource :login, only: [:create], controller: :sessions
    resource :users, only: [:create]
  end
end
