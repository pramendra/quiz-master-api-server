[http://mayurrokade.com/blog/rails-associations-beginner-pitfalls/](http://mayurrokade.com/blog/rails-associations-beginner-pitfalls/)

# tutorial

## How

* rails new api-server-quizmaster --api
* install user management using devise 
* implement tokens based authentication 
* generate scaffold

```
class V1::Question < ApplicationRecord
 -> belongs_to :exam, class_name: "V1::Exam
end


class V1::Exam < ApplicationRecord
  belongs_to :user, class_name: "User", foreign_key: "user_id"
  -> has_many :questions, class_name: "V1::Question"

end

```



## Generage Models

```
syntax rails generate scaffold NAME field[:type][:index] field[:type][:index]

$ rails g scaffold v1::exam user_id:integer name:string 

$ rails g scaffold v1::question exam_id:integer title:string disabled:boolean 

$ rails g scaffold v1::answer question_id:integer answer:string answer_type:integer 

$ rails g scaffold v1::assessment user_id:integer question_id:integer reply:string

```



## Test

### Register Account

```
curl localhost:3000/v1/users --data "email=user@example.com&password=password"
```

### Login 

```
curl localhost:3000/v1/login --data "email=user@example.com&password=password"
```

## Commands

​	_start server_

```
rails s
```

​	_create tables with seed data_

```
rake db:seed
```

​	_test_

```
rake test
```

​	_delete scaffold_

```
rails destroy scaffold v1::test
```

​	_delete scaffold_

```
rails destroy scaffold v1::test
```

​	_delete scaffold_

```
rails destroy scaffold v1::test
```

